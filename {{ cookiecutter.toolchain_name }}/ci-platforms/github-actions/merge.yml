---
name: Merges

# yamllint disable-line rule:truthy
on:
  push:
    branches:
      - {{ cookiecutter.default_branch }}

{% if cookiecutter.language == 'docker' %}
env:
  DOCKER_BUILDCONTEXT: '.'
  DOCKER_FILEPATH: 'Dockerfile'
{%- if cookiecutter.distribution_host == 'dockerhub' %}
  DOCKER_REPOSITORY: {{ "${{ secrets.DOCKER_ORGANIZATION }}" }}
{%- else %}
  DOCKER_REPOSITORY: {{ "${{ github.repository_owner }}" }}
{%- endif %}
  DOCKER_IMAGE: {{ "${{ github.event.repository.name }}" }}
{%- endif %}

jobs:
  merge:
    runs-on: ubuntu-latest
{%- if cookiecutter.language == 'java' %}
    env:
      MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
{%- endif %}
{%- if cookiecutter.language == "nodejs" %}
    strategy:
      matrix:
        node-version: [12.x, 14.x]
{% endif %}
    steps:
      - uses: actions/checkout@v2
{%- if cookiecutter.language == 'java' %}
      - name: Set Maven Options
        run:
          echo "MAVEN_OPTS=-Dmaven.repo.local=$HOME/.m2/repository" >> $GITHUB_ENV
      - name: Set up Java 1.8
        uses: actions/setup-java@v1
        with:
          java-version: 1.8
      - name: Cache Packages
        uses: actions/cache@v1
        with:
          path: ~/.m2/repository
          key: {{ "${{ runner.os }}-maven-${{ hashFiles('**/pom.xml') }}" }}
          restore-keys: |
            {{ "${{ runner.os }}-maven-" }}
      - name: Build and Run Tests
        run: mvn $MAVEN_CLI_OPTS install
      - name: Archive SNAPSHOTs
        uses: actions/upload-artifact@v1
        with:
          name: snapshot
          path: target
{%- elif cookiecutter.language == 'docker' %}
      - uses: docker/setup-buildx-action@v1
      - uses: docker/metadata-action@v3
        id: meta
        with:
          images: |
{%- if cookiecutter.distribution_host == 'github-packages' %}
            ghcr.io/{{ "${{ env.DOCKER_REPOSITORY }}/${{ env.DOCKER_IMAGE }}" }}
{%- else %}
            {{ "${{ env.DOCKER_REPOSITORY }}/${{ env.DOCKER_IMAGE }}" }}
{%- endif %}
          tags: |
            type=ref,event=branch
            type=sha
{%- if cookiecutter.distribution_host != 'none' %}
      - uses: docker/login-action@v1
        with:
{%- if cookiecutter.distribution_host == 'github-packages' %}
          username: {{ "${{ github.actor }}" }}
          password: {{ "${{ secrets.GITHUB_TOKEN }}" }}
          registry: ghcr.io
{%- elif cookiecutter.distribution_host == 'dockerhub' %}
          username: {{ "${{ secrets.DOCKER_USERNAME }}" }}
          password: {{ "${{ secrets.DOCKER_TOKEN }}" }}
          registry: docker.io
{%- endif %}
{%- endif %}
      - uses: docker/build-push-action@v2
        with:
          context: {{ "${{ env.DOCKER_BUILDCONTEXT }}" }}
          file: {{ "${{ env.DOCKER_FILEPATH }}" }}
          tags: {{ "${{ steps.meta.outputs.tags }}" }}
          push: true
{%- if cookiecutter.distribution_host == 'none' %}
      - name: Save Docker Images
        {#- TODO replace this with build-push-image tag output once added #}
        run: |
            docker save \
            docker.pkg.github.com/{{ "${{ env.DOCKER_REPOSITORY }}/${{ env.DOCKER_IMAGE }}:latest" }} \
              | gzip > container.tar.gz
      - uses: actions/upload-artifact@v1
        with:
          path: container.tar.gz
          name: {{ "${{ env.DOCKER_IMAGE }}" }}
{% endif %}
{%- elif cookiecutter.language == "nodejs" %}
      - name: Use Node.js
        uses: actions/setup-node@v1
        with:
          node-version: {{ "${{ matrix.node-version }}" }}
      - name: Cache Node.js Packages
        uses: actions/cache@v2
        with:
          path: ~/.npm
          key: {{ "${{ runner.os }}-node-${{ matrix.node-version }}-${{ hashFiles('**/package-lock.json') }}" }}
          restore-keys: {{ "${{ runner.os }}-node-${{ matrix.node-version }}" }}
      - name: Build and Install node
        run: npm ci
      - name: Create Package Tarball
        run: npm pack
      - name: Archive production artifacts
        uses: actions/upload-artifact@v2
        with:
          name: dist
          path: {{ "${{ github.workspace }}/*.tgz" }}
{% endif %}
