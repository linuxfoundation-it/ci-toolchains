---
name: Releases

# yamllint disable-line rule:truthy
on:
  push:
    tags:
{%- if cookiecutter.language == "docker" %}
      - '**'
{% elif cookiecutter.language == "java" %}
      - '[0-9]*'
{% elif cookiecutter.language == "nodejs" %}
      - '**'
{%- endif %}

{%- if cookiecutter.language == "docker" %}
env:
  DOCKER_BUILDCONTEXT: '.'
  DOCKER_FILEPATH: 'Dockerfile'
{%- if cookiecutter.distribution_host == 'dockerhub' %}
  DOCKER_REPOSITORY: {{ "${{ secrets.DOCKER_ORGANIZATION }}" }}
{%- else %}
  DOCKER_REPOSITORY: {{ "${{ github.repository_owner }}" }}
{%- endif %}
  DOCKER_IMAGE: {{ "${{ github.event.repository.name }}" }}
{%- endif %}

jobs:
  release:
    runs-on: ubuntu-latest
{%- if cookiecutter.language == "java" %}
    env:
      MAVEN_CLI_OPTS: "-s .m2/settings.xml --batch-mode"
{%- endif %}
    steps:
      - uses: actions/checkout@v2
{%- if cookiecutter.language == "java" %}
      - name: Set up Java 1.8
        uses: actions/setup-java@v1
        with:
          java-version: 1.8
      - name: Strip SNAPSHOT Version
        run: |
          mvn $MAVEN_CLI_OPTS \
            versions:set versions:update-child-modules versions:commit \
            -DnewVersion=${VERSION/refs\/tags\/} \
            -DgenerateBackupPoms=false
        env:
          VERSION: {{ "${{ github.ref }}" }}
      - name: Publish GitHub Packages
        run: |
          mvn $MAVEN_CLI_OPTS \
            deploy \
            -D altDeploymentRepository=release::default::${MAVEN_RELEASE_REPO_URL}
        env:
          MAVEN_RELEASE_REPO_USER: {{ "${{ github.actor }}" }}
          MAVEN_RELEASE_REPO_PASS: {{ "${{ secrets.GITHUB_TOKEN }}" }}
          MAVEN_RELEASE_REPO_URL: {{ "https://maven.pkg.github.com/${{ github.repository }}" }}
{%- elif cookiecutter.language == "nodejs" %}
      - name: Set up node 14.x
        uses: actions/setup-node@v1
        with:
          node-version: 14.x
{%- if cookiecutter.distribution_host == "github-packages" %}
          registry-url: {{ "https://npm.pkg.github.com/${{ github.repository_owner }}" }}
{%- elif cookiecutter.distribution_host == "npmjs" %}
          registry-url: https://registry.npmjs.org/
{%- endif %}
      - name: Build node
        run: npm ci
      - name: Publish node package
{%- if cookiecutter.distribution_host == "github-packages" %}
        run: npm publish
        env:
          NODE_AUTH_TOKEN: {{ "${{ secrets.GITHUB_TOKEN }}" }}
{%- elif cookiecutter.distribution_host == "npmjs" %}
        run: npm publish --access public
        env:
          NODE_AUTH_TOKEN: {{ "${{ secrets.NPM_ACCESS_TOKEN }}" }}
{%- endif %}
{%- elif cookiecutter.language == "docker" %}
      - uses: docker/setup-buildx-action@v1
      - uses: docker/metadata-action@v3
        id: meta
        with:
          images: |
{%- if cookiecutter.distribution_host == 'github-packages' %}
            ghcr.io/{{ "${{ env.DOCKER_REPOSITORY }}/${{ env.DOCKER_IMAGE }}" }}
{%- else %}
            {{ "${{ env.DOCKER_REPOSITORY }}/${{ env.DOCKER_IMAGE }}" }}
{%- endif %}
          tags: |
            type=ref,event=tag
            type=semver,pattern={{ "{{version}}" }}
            type=semver,pattern={{ "{{major}}.{{minor}}" }}
            type=semver,pattern={{ "{{major}}" }}
            type=sha
      - uses: docker/login-action@v1
        with:
{%- if cookiecutter.distribution_host == 'github-packages' %}
          username: {{ "${{ github.actor }}" }}
          password: {{ "${{ secrets.GITHUB_TOKEN }}" }}
          registry: ghcr.io
{%- elif cookiecutter.distribution_host == 'dockerhub' %}
          username: {{ "${{ secrets.DOCKER_USERNAME }}" }}
          password: {{ "${{ secrets.DOCKER_TOKEN }}" }}
          registry: docker.io
{%- endif %}
      - uses: docker/build-push-action@v2
        with:
          context: {{ "${{ env.DOCKER_BUILDCONTEXT }}" }}
          file: {{ "${{ env.DOCKER_FILEPATH }}" }}
          tags: {{ "${{ steps.meta.outputs.tags }}" }}
          push: true
{%- endif %}
