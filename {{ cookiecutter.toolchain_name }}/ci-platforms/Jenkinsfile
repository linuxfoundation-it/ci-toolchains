def app_image

pipeline {
    parameters {
{%- if cookiecutter.builder == 'openstack' %}
        string(name: "AGENT_NAME", defaultValue: "centos7-docker-4c-2g",
               description: "Jenkins agent label")
{%- endif %}
        string(name: "PROJECT_NAME", defaultValue: "",
               description: "Name of project/repository")
        string(name: "DOCKER_ORGANIZATION", defaultValue: "",
               description: "Name of organization or account in Docker registry")
{%- if cookiecutter.distribution_host == 'dockerhub' %}
        string(name: "DOCKERHUB_CREDENTIALS", defaultValue: "dockerhub-credentials",
               description: "Name of Docker Hub credentials in Jenkins")
{%- elif cookiecutter.distribution_host == 'github-packages' %}
        string(name: "GHCR_CREDENTIALS", defaultValue: "ghcr-credentials",
               description: "Name of GitHub Container Registry credentials in Jenkins")
{%- endif %}
    }
    agent {
{%- if cookiecutter.builder == 'openstack' %}
        label "${params.AGENT_NAME}"
{%- elif cookiecutter.builder == 'kubernetes' %}
        kubernetes {
            label 'mypod'
            defaultContainer 'jnlp'
            //cloud 'kubernetes'
            yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: ansible
    image: williamyeh/ansible:alpine3
    command:
     - /bin/sh
    env:
      - name: ANSIBLE_STDOUT_CALLBACK
        value: 'unixy'
    tty: true
"""
        }
{%- endif %}
    }

    stages {
        stage('Docker Build') {
            steps {
                script {
                    if ("${params.DOCKER_ORGANIZATION}" != "") {
                        app_image = docker.build("${params.DOCKER_ORGANIZATION}/"
                            + "${params.PROJECT_NAME}")
                    else {
                        app_image = docker.build("${params.PROJECT_NAME}")
                    }
                }
            }
        }
        stage('Docker Push PR') {
            when { // Run only on Pull/Merge Requests
                changeRequest
            }
            steps {
                script {
{%- if cookiecutter.distribution_host == 'dockerhub' %}
                    docker.withRegistry("", "${params.DOCKERHUB_CREDENTIALS}") {
{%- elif cookiecutter.distribution_host == 'github-packages' %}
                    docker.withRegistry("https://ghcr.io",
                                        "${params.GHCR_CREDENTIALS}") {
{%- endif %}
                        app_image.push("${GIT_COMMIT}")
                    }
                }
            }
        }
        stage('Docker Push Latest') {
            when { // Run only on merges to master/main
                branch "{{ cookiecutter.default_branch }}"
            }
            steps {
                script {
{%- if cookiecutter.distribution_host == 'dockerhub' %}
                    docker.withRegistry("", "${params.DOCKERHUB_CREDENTIALS}") {
{%- elif cookiecutter.distribution_host == 'github-packages' %}
                    docker.withRegistry("https://ghcr.io",
                                        "${params.GHCR_CREDENTIALS}") {
{%- endif %}
                        app_image.push("latest")
                        if ("${TAG_NAME}") {
                            app_image.push("${TAG_NAME}")
                        }
                    }
                }
            }
        }
    }
}
