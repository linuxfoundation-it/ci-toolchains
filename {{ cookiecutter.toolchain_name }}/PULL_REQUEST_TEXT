{%- set ns = namespace(change_type="", change_type_help_link="") -%}
{%- if cookiecutter.ci == 'github-actions' -%}
{%- set ns.change_type = 'pull request' -%}
{%- set ns.change_type_help_link = "https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/checking-out-pull-requests-locally" -%}
{%- elif cookiecutter.ci == 'gitlab-ci' -%}
{%- set ns.change_type = 'merge request' -%}
{%- set ns.change_type_help_link = "https://docs.gitlab.com/ee/user/project/merge_requests/reviewing_and_managing_merge_requests.html#tips"
-%}
{%- endif -%}
# CI/CD on {{ cookiecutter.ci }}

This {{ ns.change_type }} adds The Linux Foundation {{ cookiecutter.language }}
toolchain for CI/CD workflows on {{ cookiecutter.ci }}.

## Toolchain

The CI/CD workflows for this repository are triggered by the following:

{% if cookiecutter.language == 'java' -%}
* {{ ns.change_type|title }}s: Any {{ ns.change_type }} created will have 'maven install' run
  to build, test, and verify that the project can be installed to a
  local repository.
* Merges: The same workflow for {{ ns.change_type|title }}s is run but instead of
  installing to a local repository the build is saved as an artifact to GitHub.
* GitHub Releases: At any point the project can be packaged and released by
  creating a GitHub Release. This creates a maven artifact versioned to
  the specified tag of the release and upload it to {{ cookiecutter.distribution_host }}.
{% elif cookiecutter.language == 'nodejs' -%}
* Pull Requests: Any pull request created will have 'npm ci'
  run to build and verify that the project can be installed to a
  local repository.
* Merges: The same workflow for Pull Requests is run but instead of
  installing to a local repository the build is saved as an artifact using 'npm pack'.
* GitHub Releases: At any point the project can be packaged and released by
  creating a GitHub Release. This creates a node artifact versioned to
  the specified tag of the release and uploaded to {{ cookiecutter.distribution_host }}.
{% elif cookiecutter.language == 'docker' -%}
* {{ ns.change_type|title }}s: {{ ns.change_type }}s against {{ cookiecutter.default_branch }} will verify the project
  container can be built. The container won't be pushed anywhere.
* Merge: Merges to the repository build the docker container and tag it
  with ':latest', and {% if cookiecutter.distribution_host != 'none' %}
  push it to {{ cookiecutter.distribution_host }}. {% else %} save it as
  a CI artifact. {% endif %}
{% if cookiecutter.distribution_host != 'none' -%}
* Release: On tags, the container is built, tagged with both the
  specified tag and ':latest' and pushed to {{ cookiecutter.distribution_host }}.
{% endif -%}
{% endif %}

## {{ cookiecutter.language|capitalize }} Environment Variables

The following variables are available to workflows:

{%- if cookiecutter.language == 'java' %}
- `$MAVEN_RELEASE_REPO_URL`
   URL of the release repository
- `$MAVEN_RELEASE_REPO_USER`
   Username for the release repository
- `$MAVEN_RELEASE_REPO_PASS`
   Password for the release repository
{%- elif cookiecutter.language == 'docker' %}
- `$DOCKER_IMAGE`
  Name of the docker image to create
- `$DOCKER_BUILDCONTEXT`
  Path to the build context for the docker image
- `$DOCKER_FILEPATH`
  Path to the Dockerfile being built
{%- if cookiecutter.ci == "github-actions" %}
- `$DOCKER_REPOSITORY`
  Name of the docker registry repository where the image will be
  distributed
{%- endif %}
{% endif -%}

{%- if cookiecutter.artifact_host != 'none' -%}
- MAVEN_RELEASE_REPO
  Name of the release repository

- MAVEN_SNAPSHOT_REPO_URL
  URL of the snapshot repository

- MAVEN_SNAPSHOT_REPO_USER
  Username for the snapshot repository

- MAVEN_SNAPSHOT_REPO_PASS
  Password for the snapshot repository

- MAVEN_SNAPSHOT_REPO
  Name of the snapshot repository
{% endif -%}

{%- if cookiecutter.sonar_scanning == 'y' -%}

### Sonar Settings

- `$SONAR_HOST_URL`
  URL of the SonarQube instance

- `$SONAR_PROJECT_KEY`
  Key for your Sonar project

- `$SONAR_LOGIN`
  Username or API Token

{% if cookiecutter.sonar_cloud == 'y' -%}
- `$SONAR_ORGANIZATION`
  Sonarcloud.io Organization
{% endif -%}
{% endif -%} {# sonar_scanning #}

{%- if cookiecutter.documentation == 'y' -%}
## Docs Toolchain Environment

- RTD_PROJECT_NAME
    Project name on ReadTheDocs

- RTD_BUILD_URL
    URL endpoint for triggering new build

- RTD_TOKEN
    Token needed for triggering the RTD_BUILD_URL
{% endif -%}

{% if (cookiecutter.ci in ['gitlab-ci', 'circleci', 'jenkins']) %}
## Validating CI Locally

{{ cookiecutter.ci }} jobs can be verified with:

{% if cookiecutter.ci == 'gitlab-ci' -%}

  gitlab-runner exec docker verify

{% endif -%}
{% if cookiecutter.ci == 'circleci' -%}

  circleci config validate
  circleci config process .circleci/config.yml > tmp-config.yml
  circleci local execute -c tmp-config.yml --job verify

Note: The container used must contain git, and changes must be commited
to the repository.
{% endif -%}
{% if cookiecutter.ci == 'jenkins' -%}

  # curl (REST API)
  # Assuming "anonymous read access" has been enabled on your Jenkins instance.
  # JENKINS_URL=[root URL of Jenkins master]
  # JENKINS_CRUMB is needed if your Jenkins master has CRSF protection enabled as it should
  JENKINS_CRUMB=`curl "$JENKINS_URL/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)"`
  curl -X POST -H $JENKINS_CRUMB -F "jenkinsfile=<Jenkinsfile" $JENKINS_URL/pipeline-model-converter/validate

  Then copy pasting the Jenkins file to a local docker instance of
  Jenkins and running the job.

{% endif -%}
{% endif -%} {# end cookiecutter.ci #}

## Updating this {{ ns.change_type|title }}

If these workflows don't match the needs for your project feel free to
[modify]({{ ns.change_type_help_link }}) them to meet your needs.
